import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LocalNotifications } from '@ionic-native/local-notifications';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private localNotifications: LocalNotifications) { }

  normalnotification() {
    let hora = new Date();
    let dia = hora.getDate();
    let mes = hora.getMonth();
    let ano = hora.getFullYear();
    let alert1 = new Date(ano, mes, dia, 10, 0, 0)
    let alert2 = new Date(ano, mes, dia, 13, 0, 0)
    let alert3 = new Date(ano, mes, dia, 16, 0, 0)
    let alert4 = new Date(ano, mes, dia, 19, 0, 0)
    let alert5 = new Date(ano, mes, dia, 21, 0, 0)

    this.localNotifications.schedule([{
      title: 'Lembrete ativado',
      text: '10:00, 13:00, 16:00, 19:00, 21:00',
      icon: 'https://s3-sa-east-1.amazonaws.com/v2share-s3/water-9-xxl.png',
      sound: 'https://notificationsounds.com/soundfiles/35051070e572e47d2c26c241ab88307f/file-74_bells-message.mp3'
    },
    {
      title: 'Vivi hora de beber água!!',
      text: '= ) Sáaaude',
      at: alert1,
      icon: 'https://s3-sa-east-1.amazonaws.com/v2share-s3/water-9-xxl.png',
      sound: 'https://notificationsounds.com/soundfiles/35051070e572e47d2c26c241ab88307f/file-74_bells-message.mp3'
    },
    {
      title: 'Vivi hora de beber água!!',
      text: '= ) Sáaaude',
      at: alert2,
      icon: 'https://s3-sa-east-1.amazonaws.com/v2share-s3/water-9-xxl.png',
      sound: 'https://notificationsounds.com/soundfiles/35051070e572e47d2c26c241ab88307f/file-74_bells-message.mp3'
    },
    {
      title: 'Vivi hora de beber água!!',
      text: '= ) Sáaaude',
      at: alert3,
      icon: 'https://s3-sa-east-1.amazonaws.com/v2share-s3/water-9-xxl.png',
      sound: 'https://notificationsounds.com/soundfiles/35051070e572e47d2c26c241ab88307f/file-74_bells-message.mp3'
    },
    {
      title: 'Vivi hora de beber água!!',
      text: '= ) Sáaaude',
      at: alert4,
      icon: 'https://s3-sa-east-1.amazonaws.com/v2share-s3/water-9-xxl.png',
      sound: 'https://notificationsounds.com/soundfiles/35051070e572e47d2c26c241ab88307f/file-74_bells-message.mp3'
    },
    {
      title: 'Vivi hora de beber água!!',
      text: '= ) Sáaaude',
      at: alert5,
      icon: 'https://s3-sa-east-1.amazonaws.com/v2share-s3/water-9-xxl.png',
      sound: 'https://notificationsounds.com/soundfiles/35051070e572e47d2c26c241ab88307f/file-74_bells-message.mp3'
    }
    ]);
  }


}
